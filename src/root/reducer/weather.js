import * as types from "../constant";

const initialState = {
    weather: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case types.GET_WEATHER_BY_CITY_NAME:
            return {...state, weather: action.payload };
        default:
            return state;
    }
}
