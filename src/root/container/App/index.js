import React, { Component } from 'react';
import logo from './logo.svg';
import {
    SCSS_ROOT,
    SCSS_HEADER,
    SCSS_LOGO,
    SCSS_NTRO,
    SCSS_LOGO_SPIN
} from './scss/root.scss';

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
        <div className={SCSS_ROOT}>
            <div className={SCSS_HEADER}>
                <img src={logo} className={SCSS_LOGO} alt="logo" />
                <h2>Welcome to React</h2>
            </div>
            <p className={SCSS_NTRO}>
                To get started, edit <code>src/App.js</code> and save to reload.
            </p>
            {this.props.children}
        </div>
)
    }
}

export default App;
